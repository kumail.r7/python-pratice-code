#!/bin/bash

# Download the latest Conda installer script
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh

# Run the installer script
bash ~/miniconda.sh -b -p $HOME/miniconda

# Add Conda to the system PATH
echo 'export PATH="$HOME/miniconda/bin:$PATH"' | sudo tee -a /etc/profile.d/conda.sh
sudo chmod +x /etc/profile.d/conda.sh
source /etc/profile.d/conda.sh

# Initatite the conda
conda init bash

# Restart the current shell
exec "$SHELL"

# Create a new environment and activate it
conda create -n myenv
conda activate myenv


