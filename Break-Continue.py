'''
# Break Statement  # break the loop and doesnt itrate 
for i in "DevOps":
    print(i)
    if i == "O":
        print("Found the data.")
        break
print("out of loop")
'''

'''
# Continue (Skip) Statement   # Continue the loop 
for i in "DevOps":
    if i == "O":
        print("Found the data.")
        continue
    print(f"Value of i is {i}")
print("out of loop")
'''

import random
VACCINES = ["Moderna", "Pfizier" , "Spuntik V" ,"Covaxin", "CoronaVac" , "AztraZenec"]

random.shuffle(VACCINES)
print(VACCINES)

LUCKY = random.choice(VACCINES)
print(LUCKY)

for vac in VACCINES:
    print (f"*****TESTING VACCINE {vac}")
    if vac == LUCKY:
        print ("#################################")
        print (f"{LUCKY} is the best Vaccine now.")
        print ("#################################")
        print()
        continue
    print("################")
    print("Test Failed")
    print("################")
    print()              