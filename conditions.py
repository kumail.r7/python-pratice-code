# IF conditions 
"""
x = 21

if (x < 30):
    print ("Inside the IF Block")
    print ("X is less than than 30")

print  ("Rest of the code")

x = 31

if (x < 30):
    print ("Inside the IF Block")
    print ("X is less than than 30")

print  ("Rest of the code")
"""

# IF ELSE Condtions 

"""
x = 31

if (x < 30):
    print ("Inside the IF Block")
    print ("X is less than than 30")
else:
    print ("Inside the IF Block")
    print ("X is greater than 30")

print  ("Rest of the code")
"""

# IF Elif Else Condtion

x = 40 

if (x > 40 ):
   print ("X is Greater than 40")
elif (x == 40):
   print ("X is Equal to 40")

else:
   print ("X is Less than 40")