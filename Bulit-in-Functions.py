'''
# String Built in Method / Functions
message = "corona vaccine is ready to use , 80% effective"
print(message)
print(message.capitalize())
'''

# dir fuction 
#print(dir(message))

'''
# Data Validation 
print(message.upper())
print(message.islower())
print(message.isupper())
print(message.find("vaccine"))  # returns an index value 
print(message[18:24])
'''

'''
seq = ("198" ,"126","68", "79")
print(".".join(seq))
print("/".join(seq))
'''

# list mutability

cars = ["Ferrari" , "Porsche", "Mazda" , "Telsa", "Buggati", "Pagani"]
cars.append("Lamborghni")
print(cars)

cars.extend(["Ford", "Mustang"])
print(cars)

cars.insert(3 ,"Rezvan")
print(cars)

cars.pop() # deleting a var (last in the list)
print(cars)

cars.pop(4) # will remove the word on index 4
print(cars)