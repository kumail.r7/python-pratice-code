# For Loops Syntax 
'''
PLANET = "Kumail Rizvi" # This repeats over a Squence of number 

for i in PLANET:
    print(i)

print ("Rest of the code")
'''

# While Loops Syntax

x = 0 

while x <= 10:
    print ("The vaule of x is", x)
    print ("Looping")
    x+=1

print ("Stopping the code")