# Arithmetic opertors
a = 3 
b = 2 
 
total = a + b 
print(total)

total = a - b 
print(total)

total = a * b 
print (total)

total = a / b 
print (total)

total = a % b
print (total)

total = b**a
print (total)

# Comparsion Opertors 

a = 3 
b = 6

out = a > b # Greater
print (out)

out = a < b # Less
print(out)

out = a == b # Equal
print (out)

out = a != b # Reverse
print (out)

out = a >= b # Greater Than Equal
print (out)

out = a <= b # Less Than Equal
print (out)

# Assignment Opertors 

c = 0
d = 1

#c+=d # c = c + d 
print (c)

c-=d # c = c - d
print (c)

# Logical Opertors 

# and  both should be true to return a true value 
# or  if one of them is true then the output will be true

a = 20
b = 10 

x = 2
y = 3

out = (a > b) or (x > y)
print (out)

out = (a < b) or (x < y)
print (out)

out = (a > b) or (x < y)
print (out)

out = (a < b) or (x > y)
print (out)

out = (a > b) and (x > y)
print (out)

out = (a < b) and (x > y)
print (out)

out = (a > b) and (x < y)
print (out)

out = (a < b) and (x < y)
print (out)

out = not(x < y)
print (out)

# Membership Operator 

# in  is the membership Operator 

first_list =  ("IOT" , "devops" , 34 , "CI/CD")

ans = "devops" in first_list
print (ans)

ans = "devops" not in first_list
print (ans)

# Identity Opertors 

a = 12 
b = 13

result = a is b 
print (result)

result = a is not b 
print (result)