'''
# varaible Length Argument (non keyword arguments) *args (tuple)
def drinks(min_order , *args):
    print (f"the drink {min_order} will be avaliable")
    for items in args:
        print (f"the orderded drinks are : {items}")
    print("Thank you for having patience")

drinks ("coca-cola" , "sprite" , "fanta" , "Red Bull")
'''

# varaible Length Argument (keyword arguments) **kwargs (dict)
import random
def time_active(*args , **kwargs):
#    print(kwargs)
    min = sum(args) + random.randint(0,60)
#    print (min)
    choice = random.choice(list(kwargs.keys())) 
#    print (choice) 
    print (f"You Should spend {min} mins to {choice}")  




time_active(10 ,20 ,10 , hobby="Painting" , fun="football", work="Devops")