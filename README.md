
## WELCOME TO PYTHON PRACTICE REPO FOR DEVOPS TEAM 

=====================================

This repository contains a collection of Python scripts for practicing programming concepts and solving coding challenges. Each script is designed to be a standalone program that can be executed on the command line.

# Getting Started 

====================================

1) Creating an Environment via Conda for Running python programs 

Please check the `conda-install.sh` automation script and run it , this will help you to create conda environment 

2) Create your custom enivronment to run your desire code 

After successfully installation of conda on your local system , run the commands mentioned below 

  `conda create --name $(YOUR ENV NAME)` // This will Create the desire environment

  `conda activate $(YOUR ENV NAME)`  // This will activate the environment 

To Deactivate the environment run the command below 

-- `conda deactivate $(YOUR ENV NAME)`

3) Please create your Seprate branch so that will be easy for you to make changes and run your desire code 

-- Format of branch (name) 

## Usage

Each Python script is named according to the programming concept or coding challenge it's designed to practice. To run a script, navigate to the repository directory in the terminal and type:

`python scriptname.py`

For example, to run the script that practices looping, you would type:

`python loops.py`

## Contributing
Contributions are welcome! If you have a Python script that you'd like to add to this repository, please fork the repository and create a pull request with your changes.

## License
This project is licensed under the MIT License - see the LICENSE.md file for details.

## Acknowledgments
These scripts were inspired by various coding challenges and programming concepts from myself , online resources and books. Special thanks to the following sources:


Roshan (Python Instructor) <br>
Udemy <br>
HackerRank <br>
Project Euler <br>
Python for Everybody <br>
Automate the Boring Stuff with Python



