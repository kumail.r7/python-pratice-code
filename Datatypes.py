num = 45
float = 2.3 
var = "q"
str = "devops"

# list , # Collection of multiple datatype enclosed in a [] , mutable 
first_list =  [num , "devops" , 34 , var]

print (first_list)

# tuple , # Collection of multiple datatype enclosed in a () , non mutable 
first_list =  (num , "devops" , 34 , var)

print (first_list)

#Dictionary , # Collection of multiple datatype enclosed in a {} in form of key value pairs 
first_list =  {"Name":"Kumail" ,"Role":"Devops" ,"Age":"26"}

print (first_list)

# Boolean 
x = True
y = False

print ("x is a" , x)
print ("y is a" , y) 
