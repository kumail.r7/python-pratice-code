name = "sars_cov_2"
disease = "covid_19"

print ("The name of the virus is ", name)
print ("the name of the virus is {}" .format(name))
print ("{} is the name of the virus" .format(name))

print ("the name of the virus is {} and it causes {}" .format(name , disease))

print (f"the name of the virus is {name} and it is causes {disease}") # works in python 3 or higher 

# Concatination 
print ("The name of the virus is " + "" + name)