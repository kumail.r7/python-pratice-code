x=1000

print("Learning Indentation")
print()

if x == 0:
    print ("In the IF Block")
    print ("Value of x is 0")
else:
    print ("In the ELSE Block")
    print ("Value of x is non-zero")

    print()
    print("This statement is out of if/else statements")